package com.example.countryinfo.json;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Countries;
import com.example.countryinfo.data.States;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetAllCountries extends AsyncTaskLoader<List<Countries>> {

    List<Countries> countries = new ArrayList<>();
    HttpURLConnection urlConnection = null;
    String BASE_URL = Constants.BASE_URL_ALL_COUNTRIES;
    BufferedReader reader = null;
    String resultJson = "";
    static final String NAME = "name";
    static final String CODE = "code";
    static final String STATES = "states";
    static final String RESULT = "result";
    static final String NO_STATES = "null";

    public GetAllCountries(Context context) {
        super(context);
    }

    @Override
    public List<Countries> loadInBackground() {
        try {
            URL url = new URL(BASE_URL);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            resultJson = buffer.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        List<States> states;
        try {
            JSONArray result = new JSONObject(resultJson).getJSONArray(RESULT);

            for (int i = 0; i < result.length(); i++) {
                JSONObject jsonCountries = result.getJSONObject(i);
                String nameCountry = jsonCountries.getString(NAME);
                String codeCountry = jsonCountries.getString(CODE);
                String state = jsonCountries.getString(STATES);

                if (!state.equals(NO_STATES)) {
                    states = new ArrayList<>();
                    JSONArray jsonStates = jsonCountries.getJSONArray(STATES);

                    for (int j = 0; j < jsonStates.length(); j++) {
                        JSONObject statesDetails = jsonStates.getJSONObject(j);
                        String nameStates = statesDetails.getString(NAME);
                        String codeStates = statesDetails.getString(CODE);
                        states.add(new States(nameStates, codeStates));

                    }
                } else {
                    states = new ArrayList<>();
                }
                countries.add(new Countries(nameCountry, codeCountry, states));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countries;
    }

}