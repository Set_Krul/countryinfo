package com.example.countryinfo.json;

import android.content.Context;

import com.example.countryinfo.R;
import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Details;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

public class GetCountryDetails {

    HttpURLConnection urlConnection = null;
    String BASE_URL = Constants.BASE_URL_DETAILS_COUNTRY;
    BufferedReader reader = null;
    String resultJson = "";
    String codeCountry = "";
    private String Lat = "", Lon = "", West = "", East = "", North = "", South = "";
    Context mContext;
    Details details = new Details();
    static final String RESULTS = "results";
    static final String GEOMETRY = "geometry";
    static final String VIEWPORT = "viewport";
    static final String NORTHEAST = "northeast";
    static final String SOUTHWEST = "southwest";
    static final String LOCATION = "location";
    static final String LAT = "lat";
    static final String LNG = "lng";


    public GetCountryDetails(Context context, String codeCountry) {
        this.mContext = context;
        this.codeCountry = codeCountry;
    }

    public Details getData() {
        try {
            URL url = new URL(BASE_URL + codeCountry);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            resultJson = buffer.toString();
            try {
                JSONArray result = new JSONObject(resultJson).getJSONArray(RESULTS);
                JSONObject jsonCountries = result.getJSONObject(0);
                String base_parse = jsonCountries.getString(GEOMETRY);
                North = getSubString(base_parse, new String[]{VIEWPORT, NORTHEAST, LAT});
                details.setNorth(roundMath(North));
                East = getSubString(base_parse, new String[]{VIEWPORT, NORTHEAST, LNG});
                details.setEast(roundMath(East));
                South = getSubString(base_parse, new String[]{VIEWPORT, SOUTHWEST, LAT});
                details.setSouth(roundMath(South));
                West = getSubString(base_parse, new String[]{VIEWPORT, SOUTHWEST, LNG});
                details.setWest(roundMath(West));
                Lat = getSubString(base_parse, new String[]{LOCATION, LAT});
                details.setLat(roundMath(Lat));
                Lon = getSubString(base_parse, new String[]{LOCATION, LNG});
                details.setLon(roundMath(Lon));
            } catch (JSONException e) {
                e.printStackTrace();
                details.setNorth("");
                details.setEast("");
                details.setSouth("");
                details.setWest("");
                details.setLat("");
                details.setLon("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    private String roundMath(String math) {
        if (!math.equals("")) {
            return " " + new DecimalFormat("0.00").format(Double.parseDouble(math)).replace(',', '.');
        } else {
            return " " + mContext.getString(R.string.isnt_found_coordinates);
        }
    }

    private String getSubString(String base, String[] sub) throws JSONException {
        for (int i = 0; sub.length > i; i++) {
            JSONObject a = new JSONObject(base);
            base = a.getString(sub[i]);
        }
        return base;
    }
}
