package com.example.countryinfo.ui;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.example.countryinfo.R;
import com.example.countryinfo.adapter.ExpandableListAdapter;
import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Countries;
import com.example.countryinfo.data.States;
import com.example.countryinfo.db.DatabaseHelper;


public class SavedCountiesFragment extends Fragment implements SearchView.OnQueryTextListener, SearchView
        .OnCloseListener {

    private SearchView mSearchView;
    private ExpandableListAdapter mAdapter;
    private ExpandableListView mExpandableListView;
    private String query = "";
    DatabaseHelper db;

    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", query);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            query = savedInstanceState.getString("query");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = new DatabaseHelper(getActivity());
        setRetainInstance(true);
        rootView = inflater.inflate(R.layout.fragment_saved, container, false);
        setHasOptionsMenu(true);
        mExpandableListView = (ExpandableListView) rootView.findViewById(R.id.expListViewSave);
        mAdapter = new ExpandableListAdapter(rootView.getContext(), db.getCountiesList());
        mExpandableListView.setAdapter(mAdapter);
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                String nameStates = ((States) mAdapter.getChild(groupPosition, childPosition)).getName();
                String codeStates = ((States) mAdapter.getChild(groupPosition, childPosition)).getCode();
                Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.STATES);
                i.putExtra(Constants.SAVED, Constants.SAVED_YES);
                i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                i.putExtra(Constants.NAME_STATES, nameStates);
                i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                i.putExtra(Constants.CODE_STATES, codeStates);
                startActivity(i);
                return false;
            }
        });

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                int groupSize = (((Countries) mAdapter.getGroup(groupPosition)).getStates()).size();
                if (groupSize == 0) {
                    String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                    String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                    Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                    i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.COUNTRY);
                    i.putExtra(Constants.SAVED, Constants.SAVED_YES);
                    i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                    i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                    startActivity(i);
                }
                return false;
            }
        });
        mExpandableListView.setOnItemLongClickListener(new ExpandableListView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int groupPosition, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView
                        .PACKED_POSITION_TYPE_GROUP) {
                    String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                    String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                    Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                    i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.COUNTRY);
                    i.putExtra(Constants.SAVED, Constants.SAVED_YES);
                    i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                    i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                    startActivity(i);
                }
                return false;
            }
        });

        //rightGroupIndicator();
        return rootView;
    }

    public void rightGroupIndicator() {
        int width;
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) rootView.getContext().getSystemService(Context
                .WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;

        if (android.os.Build.VERSION.SDK_INT <
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mExpandableListView.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        } else {
            mExpandableListView.setIndicatorBoundsRelative(width - GetDipsFromPixel(50),
                    width - GetDipsFromPixel(10));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        if (!query.equals("")) {
            mSearchView.setQuery(query, true);
            mSearchView.setFocusable(true);
            mSearchView.setIconified(false);
            mSearchView.requestFocusFromTouch();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                mSearchView.setIconified(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public int GetDipsFromPixel(float pixels) {
        final float scale = rootView.getContext().getResources().getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public boolean onClose() {
        query = "";
        mAdapter.filterData("");
        collapseAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.query = query;
        mAdapter.filterData(query);
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        this.query = query;
        mAdapter.filterData(query);
        expandAll();
        return false;
    }

    private void expandAll() {
        int count = mAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            mExpandableListView.expandGroup(i);
        }
    }

    private void collapseAll() {
        int count = mAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            mExpandableListView.collapseGroup(i);
        }
    }
}