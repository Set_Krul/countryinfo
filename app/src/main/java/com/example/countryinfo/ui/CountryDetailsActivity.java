package com.example.countryinfo.ui;


import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.countryinfo.MainActivity;
import com.example.countryinfo.R;
import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Details;
import com.example.countryinfo.db.DatabaseHelper;
import com.example.countryinfo.dialogs.Dialogs;
import com.example.countryinfo.image.GetFlag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class CountryDetailsActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager
        .LoaderCallbacks<Details> {
    private Toolbar mToolbar;
    private String id;
    private String saved;
    private String nameCountry;
    private String codeCountry;
    private String nameStates;
    private String codeStates;
    private String title;
    private String flag;
    private static final int LOADER_FLAG = 1;

    private ProgressDialog pDialog;
    TextView tvLat, tvLon, tvWest, tvEast, tvNorth, tvSouth;
    ImageView ivFlag;
    Button btnGoMap;
    Details details = new Details();
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_deteils);
        db = new DatabaseHelper(this);
        pDialog = new ProgressDialog(this);
        Bundle extras = getIntent().getExtras();
        id = (extras != null) ? extras.getString(Constants.ID_COUNTRY_OR_STATES) : "";
        saved = (extras != null) ? extras.getString(Constants.SAVED) : "";
        nameCountry = (extras != null) ? extras.getString(Constants.NAME_COUNTRY) : "";
        codeCountry = (extras != null) ? extras.getString(Constants.CODE_COUNTRY) : "";
        nameStates = (extras != null) ? extras.getString(Constants.NAME_STATES) : "";
        codeStates = (extras != null) ? extras.getString(Constants.CODE_STATES) : "";
        if (saved.equals(Constants.SAVED_YES)) {
            details = db.getDetailsCountry(id.equals(Constants.COUNTRY) ? codeCountry : codeStates, id.equals(Constants.COUNTRY) ? 0
                    : 1);
        }
        if (id.equals(Constants.COUNTRY)) {
            title = nameCountry;
            flag = codeCountry;
            details.setStName("");
            details.setStCode("");
        } else {
            title = nameCountry + " - " + nameStates;
            flag = codeStates;
            details.setStName(nameStates);
            details.setStCode(codeStates);
        }

        details.setName(nameCountry);
        details.setCode(codeCountry);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(title.toUpperCase());

        ivFlag = (ImageView) findViewById(R.id.ivFlag);
        tvLat = (TextView) findViewById(R.id.tvLat);
        tvLon = (TextView) findViewById(R.id.tvLon);
        tvWest = (TextView) findViewById(R.id.tvWest);
        tvEast = (TextView) findViewById(R.id.tvEast);
        tvNorth = (TextView) findViewById(R.id.tvNorth);
        tvSouth = (TextView) findViewById(R.id.tvSouth);
        btnGoMap = (Button) findViewById(R.id.btnGoMap);
        btnGoMap.setOnClickListener(this);
        if (saved.equals(Constants.SAVED_YES)) {
            setFlag();
            tvLat.setText(getString(R.string.coordinates_lat) + " " + (details.getLat().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getLat()));
            tvLon.setText(getString(R.string.coordinates_lon) + " " + (details.getLon().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getLon()));
            tvWest.setText(getString(R.string.coordinates_west) + " " + (details.getWest().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getWest()));
            tvEast.setText(getString(R.string.coordinates_east) + " " + (details.getEast().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getEast()));
            tvNorth.setText(getString(R.string.coordinates_north) + " " + (details.getNorth().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getNorth()));
            tvSouth.setText(getString(R.string.coordinates_south) + " " + (details.getSouth().equals("") ? getString(R
                    .string.isnt_found_coordinates) : details.getSouth()));
        } else {
            Bundle bndl = new Bundle();
            bndl.putString(Constants.CODE_COUNTRY, flag);
            getLoaderManager().initLoader(LOADER_FLAG, bndl, this);
            Loader<Object> loader = getLoaderManager().getLoader(LOADER_FLAG);
            loader.forceLoad();
        }
    }

    public void setFlag() {
        File DIRECTORYS = new File(Environment.getExternalStorageDirectory(), Constants.FLAG_FOLDER);
        File PATHS = new File(DIRECTORYS, (id.equals(Constants.COUNTRY) ? codeCountry : codeStates) + Constants.EXTENSION_FILE);
        InputStream ins = null;
        try {
            ins = new FileInputStream(PATHS);
            Bitmap bitmaps = BitmapFactory.decodeStream(ins);
            ivFlag.setImageBitmap(bitmaps);
        } catch (FileNotFoundException e) {
            ivFlag.setImageResource(R.drawable.image_no_found);
            e.printStackTrace();
        }
    }

    @Override
    public Loader<Details> onCreateLoader(int id, Bundle args) {
        Loader<Details> loader = null;
        if (id == LOADER_FLAG) {
            loader = new GetFlag(this, args);
            pDialog.setMessage(getString(R.string.loading_list));
            pDialog.setCancelable(false);
            pDialog.show();
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Details> loader, Details data) {
        pDialog.dismiss();
        ivFlag.setImageBitmap(data.getFlag() != null ? data.getFlag() : BitmapFactory.decodeResource(getResources(),
                R.drawable.image_no_found));
        tvLat.setText(getString(R.string.coordinates_lat) + " " + (data.getLat().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getLat()));
        tvLon.setText(getString(R.string.coordinates_lon) + " " + (data.getLon().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getLon()));
        tvWest.setText(getString(R.string.coordinates_west) + " " + (data.getWest().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getWest()));
        tvEast.setText(getString(R.string.coordinates_east) + " " + (data.getEast().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getEast()));
        tvNorth.setText(getString(R.string.coordinates_north) + " " + (data.getNorth().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getNorth()));
        tvSouth.setText(getString(R.string.coordinates_west) + " " + (data.getSouth().equals("") ? getString(R.string
                .isnt_found_coordinates) : data.getSouth()));
        details.setLat(data.getLat());
        details.setLon(data.getLon());
        details.setEast(data.getEast());
        details.setWest(data.getWest());
        details.setSouth(data.getSouth());
        details.setNorth(data.getNorth());
    }

    @Override
    public void onLoaderReset(Loader<Details> loader) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (saved.equals(Constants.SAVED_NO)) {
            getMenuInflater().inflate(R.menu.menu_country_details, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_country_details_saved, menu);
        }
        return true;
    }
    public void onBackPressed() {
        if (isTaskRoot()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (saved.equals(Constants.SAVED_NO)) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    onBackPressed();
                    break;

                case R.id.action_save:
                    if (!details.getLat().equals("")) {
                        if (db.dublicateCountry(details)) {
                            saveFlag(flag);
                            db.addCountry(details);
                            Toast.makeText(this, getString(R.string.country_is_saved), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, getString(R.string.country_is_already_saved), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        new Dialogs(this, this).saveError();
                    }
                    break;
            }
        } else {
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGoMap:
                Intent i = new Intent(this, MapsActivity.class);
                if (!details.getLat().equals("")) {
                    i.putExtra(Constants.COORDINATES_LAT, details.getLat());
                    i.putExtra(Constants.COORDINATES_LON, details.getLon());
                    i.putExtra(Constants.NAME_COUNTRY, title);
                    startActivity(i);
                } else {
                    new Dialogs(this, this).mapError();
                }
                break;
        }
    }

    private void saveFlag(String code) {
        File DIRECTORY = new File(Environment.getExternalStorageDirectory(), Constants.FLAG_FOLDER);
        createFolder(DIRECTORY);
        File PATH = new File(DIRECTORY, code + Constants.EXTENSION_FILE);

        ivFlag.buildDrawingCache();
        Bitmap bitmap = ivFlag.getDrawingCache();

        OutputStream fOut = null;
        try {
            fOut = new FileOutputStream(PATH);
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
            } catch (Exception e) {
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean createFolder(File folder) {
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        return success;
    }
}