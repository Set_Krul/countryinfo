package com.example.countryinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.example.countryinfo.R;
import com.example.countryinfo.adapter.ExpandableListAdapter;
import com.example.countryinfo.app.Constants;
import com.example.countryinfo.app.Singleton;
import com.example.countryinfo.data.Countries;
import com.example.countryinfo.data.States;
import com.example.countryinfo.json.GetAllCountries;

import java.util.List;

public class AllCountriesFragment extends Fragment implements SearchView.OnQueryTextListener, SearchView
        .OnCloseListener, LoaderManager.LoaderCallbacks<List<Countries>>, SwipeRefreshLayout.OnRefreshListener {

    private SearchView mSearchView;
    private ExpandableListAdapter mAdapter;
    private ExpandableListView mExpandableListView;
    private List<Countries> countries;
    static final int LOADER_TIME_ID = 1;
    int refresh = 0;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String query = "";

    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        super.onResume();
        //spike
        if (Singleton.getInstance().getListMovie().size() != 0) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", query);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            query = savedInstanceState.getString("query");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        rootView = inflater.inflate(R.layout.fragment_all_countries, container, false);
        setHasOptionsMenu(true);
        mExpandableListView = (ExpandableListView) rootView.findViewById(R.id.expListView);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        countries = Singleton.getInstance().getListMovie();
        mAdapter = new ExpandableListAdapter(rootView.getContext(), countries);
        mExpandableListView.setAdapter(mAdapter);
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                String nameStates = ((States) mAdapter.getChild(groupPosition, childPosition)).getName();
                String codeStates = ((States) mAdapter.getChild(groupPosition, childPosition)).getCode();
                Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.STATES);
                i.putExtra(Constants.SAVED, Constants.SAVED_NO);
                i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                i.putExtra(Constants.NAME_STATES, nameStates);
                i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                i.putExtra(Constants.CODE_STATES, codeStates);
                startActivity(i);
                return false;
            }
        });

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                int groupSize = (((Countries) mAdapter.getGroup(groupPosition)).getStates()).size();
                if (groupSize == 0) {
                    String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                    String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                    Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                    i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.COUNTRY);
                    i.putExtra(Constants.SAVED, Constants.SAVED_NO);
                    i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                    i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                    startActivity(i);
                }
                return false;
            }
        });
        mExpandableListView.setOnItemLongClickListener(new ExpandableListView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int groupPosition, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    String nameCountry = (((Countries) mAdapter.getGroup(groupPosition)).getName());
                    String codeCountry = (((Countries) mAdapter.getGroup(groupPosition)).getCode());
                    Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                    i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.COUNTRY);
                    i.putExtra(Constants.SAVED, Constants.SAVED_NO);
                    i.putExtra(Constants.NAME_COUNTRY, nameCountry);
                    i.putExtra(Constants.CODE_COUNTRY, codeCountry);
                    startActivity(i);
                }
                return false;
            }
        });

        //rightGroupIndicator();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeInStart();
        getLoaderManager().initLoader(LOADER_TIME_ID, new Bundle(), this);
        Loader<Object> loader = getLoaderManager().getLoader(LOADER_TIME_ID);
        loader.forceLoad();

        return rootView;
    }

    public void swipeInStart() {
        TypedValue typed_value = new TypedValue();
        rootView.getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, typed_value, true);
        swipeRefreshLayout.setProgressViewOffset(false, 0, getResources().getDimensionPixelSize(typed_value
                .resourceId));
        if (!swipeRefreshLayout.isEnabled())
            swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(true);
    }

    public void rightGroupIndicator() {
        int width;
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) rootView.getContext().getSystemService(Context
                .WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;

        if (android.os.Build.VERSION.SDK_INT <
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mExpandableListView.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        } else {
            mExpandableListView.setIndicatorBoundsRelative(width - GetDipsFromPixel(50),
                    width - GetDipsFromPixel(10));
        }
    }

    @Override
    public void onLoadFinished(Loader<List<Countries>> loader, List<Countries> result) {
        Singleton.getInstance().setListMovie(result);
        //for turn
        if (refresh == 0) {
            mAdapter = new ExpandableListAdapter(rootView.getContext(), result);
            countries = result;
            mExpandableListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
            refresh = 2;
        }
        //for refresh
        if (refresh == 1) {
            mExpandableListView = (ExpandableListView) rootView.findViewById(R.id.expListView);
            swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setRefreshing(true);
            swipeRefreshLayout.setOnRefreshListener(this);
            mAdapter = new ExpandableListAdapter(rootView.getContext(), result);
            countries = result;
            mExpandableListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
            refresh = 0;
        }
        //expandAll();
    }

    @Override
    public void onRefresh() {
        refresh = 1;
        Loader<List<Countries>> loader;
        loader = getLoaderManager().restartLoader(LOADER_TIME_ID, new Bundle(), this);
        loader.forceLoad();
    }

    @Override
    public Loader<List<Countries>> onCreateLoader(int id, Bundle args) {
        AsyncTaskLoader<List<Countries>> loader = null;
        if (id == LOADER_TIME_ID) {
            loader = new GetAllCountries(getActivity());
        }
        return loader;
    }

    @Override
    public void onLoaderReset(Loader<List<Countries>> loader) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        if (!query.equals("")) {
            mSearchView.setQuery(query, true);
            mSearchView.setFocusable(true);
            mSearchView.setIconified(false);
            mSearchView.requestFocusFromTouch();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                mSearchView.setIconified(false);
                return true;

            case R.id.action_settings:
                return true;

            case R.id.action_save:
                Intent i = new Intent(rootView.getContext(), CountryDetailsActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public int GetDipsFromPixel(float pixels) {
        final float scale = rootView.getContext().getResources().getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public boolean onClose() {
        query = "";
        mAdapter.filterData("");
        collapseAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.query = query;
        mAdapter.filterData(query);
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        this.query = query;
        mAdapter.filterData(query);
        expandAll();
        return false;
    }

    private void expandAll() {
        int count = mAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            mExpandableListView.expandGroup(i);
        }
    }

    private void collapseAll() {
        int count = mAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            mExpandableListView.collapseGroup(i);
        }
    }
}