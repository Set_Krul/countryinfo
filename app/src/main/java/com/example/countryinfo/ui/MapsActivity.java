package com.example.countryinfo.ui;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.countryinfo.R;
import com.example.countryinfo.app.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener {

    private GoogleMap mMap;
    private Toolbar mToolbar;
    private String lat;
    private String lon;
    private String nameCountry;
    private Location countryLocation = new Location("a");
    private Location mLocation;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extras = getIntent().getExtras();
        lat = (extras != null) ? extras.getString(Constants.COORDINATES_LAT) : "";
        lon = (extras != null) ? extras.getString(Constants.COORDINATES_LON) : "";
        nameCountry = (extras != null) ? extras.getString(Constants.NAME_COUNTRY) : "";


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(nameCountry.toUpperCase());


        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
    }

    private void getLocation() {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.google_play_is_not_supported), Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }

    @Override
    public void onConnected(Bundle arg0) {
        getLocation();
        if (mLocation != null) {
            setUpMapIfNeeded();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                //onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        if (mLocation != null) {
            setUpMapIfNeeded();
        }
    }


    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
        if (mMap != null) {
            getSupportFragmentManager().findFragmentById(R.id.map).setRetainInstance(true);
            setUpMap();
        }
    }

    private void setUpMap() {
        countryLocation.setLatitude(Double.parseDouble(lat.replace(',', '.')));
        countryLocation.setLongitude(Double.parseDouble(lon.replace(',', '.')));

        //country
        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat.replace(',', '.')), Double
                .parseDouble(lon.replace(',', '.'))))
                .title(nameCountry));

        //my location marker
        mMap.addMarker(new MarkerOptions().position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude())).title
                (getString(R.string.from_me_to) + " " + nameCountry + " - " + (int) countryLocation.distanceTo
                        (mLocation) / 1000 + " " + getString(R.string.km)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.human)))
        .showInfoWindow();

        //line
        PolylineOptions polylineOptions = new PolylineOptions()
                .add(new LatLng(Double.parseDouble(lat.replace(',', '.')), Double
                        .parseDouble(lon.replace(',', '.')))).add(new LatLng(mLocation.getLatitude(), mLocation
                        .getLongitude())).color(Color.RED).width(3);
        mMap.addPolyline(polylineOptions);

        CameraPosition cameraPosition2 = new CameraPosition.Builder()
                .target(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
                .zoom(0)
                .build();
        CameraUpdate cameraUpdate2 = CameraUpdateFactory.newCameraPosition(cameraPosition2);
        mMap.animateCamera(cameraUpdate2);

        //my location circle
        CircleOptions circleOptions = new CircleOptions()
                .center(new LatLng(mLocation.getLatitude(), mLocation
                        .getLongitude())).radius(250000)
                .fillColor(Color.WHITE).strokeColor(Color.BLUE)
                .strokeWidth(2);
        mMap.addCircle(circleOptions);

        //my location circle
        CircleOptions circleOptions1 = new CircleOptions()
                .center(new LatLng(mLocation.getLatitude(), mLocation
                        .getLongitude())).radius(75000)
                .fillColor(Color.BLUE).strokeColor(Color.TRANSPARENT)
                .strokeWidth(3);
        mMap.addCircle(circleOptions1);
    }
}
