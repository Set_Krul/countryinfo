package com.example.countryinfo.app;

public class Constants {
    //country save/load
    public static final String ID_COUNTRY_OR_STATES = "id";
    public static final String COUNTRY = "country";
    public static final String STATES = "states";
    public static final String COORDINATES_LAT = "lat";
    public static final String COORDINATES_LON = "lon";
    public static final String SAVED = "saved";
    public static final String SAVED_YES = "yes";
    public static final String SAVED_NO = "no";
    public static final String NAME_COUNTRY = "nameCountry";
    public static final String CODE_COUNTRY = "codeCountry";
    public static final String NAME_STATES = "nameStates";
    public static final String CODE_STATES = "codeStates";
    //flags folder
    public static final String FLAG_FOLDER = "cacheFlags";

    //flars file extension
    public static final String EXTENSION_FILE = ".gif";

    //Base url
    public static final String BASE_URL_FLAGS = "http://www.geonames.org/flags/x/";
    public static final String BASE_URL_ALL_COUNTRIES = "https://api.theprintful.com/countries";
    public static final String BASE_URL_DETAILS_COUNTRY = "http://maps.google.com/maps/api/geocode/json?components=country:";

    public static final String LOG_TAG = "myLogs";
}
