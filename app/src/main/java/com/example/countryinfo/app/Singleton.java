package com.example.countryinfo.app;

import com.example.countryinfo.data.Countries;

import java.util.ArrayList;
import java.util.List;


public class Singleton {
    private static Singleton mInstance;
    private List<Countries> countries;
    private int showLastVisite;

    private Singleton() {
        countries = new ArrayList<>();
        showLastVisite = 0;
    }

    public static void initInstance() {
        if (mInstance == null) {
            mInstance = new Singleton();

        }
    }

    public static Singleton getInstance() {
        return mInstance;
    }

    public List<Countries> getListMovie() {
        return countries;
    }

    public void setListMovie(List<Countries> countries) {
        this.countries = countries;
    }

    public int getShowLastVisite() {
        return showLastVisite;
    }

    public void setShowLastVisite(int showLastVisite) {
        this.showLastVisite = showLastVisite;
    }
}
