package com.example.countryinfo.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.countryinfo.MainActivity;
import com.example.countryinfo.R;
import com.example.countryinfo.ui.CountryDetailsActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MyReceiver extends BroadcastReceiver {

    NotificationManager nm;
    Intent i;
    PendingIntent pIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            context.startService(new Intent(context, MyService.class));
        }
        nm = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Log.d(Constants.LOG_TAG, "onReceive");
        Log.d(Constants.LOG_TAG, "action = " + intent.getAction());
        if (intent.getAction().equals(Constants.SAVED_YES)) {
            i = new Intent(context, CountryDetailsActivity.class);
            i.putExtra(Constants.ID_COUNTRY_OR_STATES, Constants.COUNTRY);
            i.putExtra(Constants.SAVED, Constants.SAVED_YES);
            i.putExtra(Constants.NAME_COUNTRY, intent.getStringExtra(Constants.NAME_COUNTRY));
            i.putExtra(Constants.CODE_COUNTRY, intent.getStringExtra(Constants.CODE_COUNTRY));
            pIntent = PendingIntent.getActivity(context, 0, i, 0);

            Bitmap bitmaps;
            File DIRECTORYS = new File(Environment.getExternalStorageDirectory(), Constants.FLAG_FOLDER);
            File PATHS = new File(DIRECTORYS, intent.getStringExtra(Constants.CODE_COUNTRY) + Constants.EXTENSION_FILE);
            InputStream ins;
            try {
                ins = new FileInputStream(PATHS);
                bitmaps = BitmapFactory.decodeStream(ins);
            } catch (FileNotFoundException e) {
                bitmaps = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.image_no_found);
                e.printStackTrace();
            }

            Resources res = context.getResources();
            int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
            int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(bitmaps.createScaledBitmap(bitmaps, width, height, false))
                    .setTicker(context.getString(R.string.notification_save) + " " + intent.getStringExtra
                            (Constants.NAME_COUNTRY))
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(context.getString(R.string.notification_save) + " " + intent.getStringExtra
                            (Constants.NAME_COUNTRY))
                    .setContentIntent(pIntent);
            nm.notify(1, mBuilder.build());

        } else {
            i = new Intent(context, MainActivity.class);
            pIntent = PendingIntent.getActivity(context, 0, i, 0);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setTicker(context.getString(R.string.notification_not_save))
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(context.getString(R.string.notification_not_save))
                    .setContentIntent(pIntent);
            nm.notify(1, mBuilder.build());
        }
    }
}
