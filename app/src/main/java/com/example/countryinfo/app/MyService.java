package com.example.countryinfo.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.countryinfo.db.DatabaseHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class MyService extends Service {

    AlarmManager am;
    DatabaseHelper db;
    PendingIntent pIntent;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = new DatabaseHelper(this);
        am = (AlarmManager) getSystemService(ALARM_SERVICE);
        Log.d(Constants.LOG_TAG, "onCREATE");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "onStart");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME, calendar.getTimeInMillis(), 24*60*60*1000 , createIntent());
        //am.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 3000, 5000, createIntent());
        return super.onStartCommand(intent, flags, startId);
    }

    PendingIntent createIntent() {
        am.cancel(pIntent);
        ArrayList<String> details;
        Intent intent = new Intent(this, MyReceiver.class);
        details = db.getAlarmCountryList();
        if (details.size() > 0) {
            intent.setAction(Constants.SAVED_YES);
            intent.putExtra(Constants.NAME_COUNTRY, details.get(0));
            intent.putExtra(Constants.CODE_COUNTRY, details.get(1));
        } else {
            intent.setAction(Constants.SAVED_NO);
        }
        pIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pIntent;
    }


    public IBinder onBind(Intent arg0) {
        return null;
    }
}
