package com.example.countryinfo.image;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Details;
import com.example.countryinfo.json.GetCountryDetails;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetFlag extends AsyncTaskLoader<Details> {

    String BASE_URL = Constants.BASE_URL_FLAGS;
    String codeCountry = "";
    HttpURLConnection urlConnection = null;
    Bitmap bitimage = null;
    Context mContext;
    Details details = new Details();


    public GetFlag(Context context, Bundle args) {
        super(context);
        this.mContext = context;
        codeCountry = args.getString(Constants.CODE_COUNTRY);
    }

    @Override
    public Details loadInBackground() {
        details = new GetCountryDetails(mContext, codeCountry).getData();
        try {
            URL url = new URL(BASE_URL + codeCountry.toLowerCase() + Constants.EXTENSION_FILE);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();

            bitimage = BitmapFactory.decodeStream(inputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }
        details.setFlag(bitimage);
        return details;
    }
}