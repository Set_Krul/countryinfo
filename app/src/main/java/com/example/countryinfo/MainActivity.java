package com.example.countryinfo;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.countryinfo.app.Singleton;
import com.example.countryinfo.db.DatabaseHelper;
import com.example.countryinfo.dialogs.Dialogs;
import com.example.countryinfo.drawer.FragmentDrawer;
import com.example.countryinfo.ui.AllCountriesFragment;
import com.example.countryinfo.ui.SavedCountiesFragment;

import java.util.Locale;


public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private boolean doubleBackToExitPressedOnce = false;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Singleton.getInstance().getShowLastVisite() == 0) {
            db = new DatabaseHelper(this);
            new Dialogs(this, this).lastVisit(db.getLastVisit());
            db.setLastVisit(getDateTime());
            Singleton.getInstance().setShowLastVisite(1);
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout),
                mToolbar);
        drawerFragment.setDrawerListener(this);
        if (savedInstanceState == null) {
            displayView(1);
        }

    }

    public String getDateTime() {
        return new java.text.SimpleDateFormat("dd LLLL yy - HH:mm", Locale.ENGLISH).format(java.util.Calendar
                .getInstance().getTime());
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new SavedCountiesFragment();
                fragment.setRetainInstance(true);
                title = getString(R.string.navigation_item_saved);
                break;
            case 1:
                if (new Dialogs(this, this).isOnline()) {
                    fragment = new AllCountriesFragment();
                    fragment.setRetainInstance(true);
                    title = getString(R.string.navigation_item_all_countries);
                }
                break;
            case 2:
                new Dialogs(this, this).showExit();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Singleton.getInstance().setShowLastVisite(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.doubleBackToExitPressedOnce, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}