package com.example.countryinfo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.example.countryinfo.R;
import com.example.countryinfo.app.Constants;
import com.example.countryinfo.data.Countries;
import com.example.countryinfo.data.Details;
import com.example.countryinfo.data.States;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DatabaseHelper extends SQLiteOpenHelper implements BaseColumns {

    private String first_visit;
    private String did_not_saved;
    private String not_found;

    private SQLiteDatabase db;

    private static final String DATABASE_NAME = "countries.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_COUNTRIES = "countries";
    private static final String COUNTRIES_ID = "_id";
    private static final String COUNTRIES_NAME = "country";
    private static final String COUNTRIES_CODE = "code_country";
    private static final String COUNTRIES_STATES_NAME = "states";
    private static final String COUNTRIES_STATES_CODE = "code_states";
    private static final String COUNTRIES_LAT = "lat";
    private static final String COUNTRIES_LON = "lon";
    private static final String COUNTRIES_WEST = "west";
    private static final String COUNTRIES_EAST = "east";
    private static final String COUNTRIES_NORTH = "north";
    private static final String COUNTRIES_SOUTH = "south";

    private static final String TABLE_LAST_VISIT = "visit";
    private static final String LAST_VISIT_ID = "_id";
    private static final String LAST_VISIT_DATATIME = "data_time";

    private static final String CREATE_TABLE_COUNTRIES;

    static {
        CREATE_TABLE_COUNTRIES = "create table " + TABLE_COUNTRIES + " ("
                + BaseColumns._ID + " integer primary key autoincrement, "
                + COUNTRIES_NAME + " text, "
                + COUNTRIES_CODE + " text, "
                + COUNTRIES_STATES_NAME + " text, "
                + COUNTRIES_STATES_CODE + " text, "
                + COUNTRIES_LAT + " text, "
                + COUNTRIES_LON + " text, "
                + COUNTRIES_WEST + " text, "
                + COUNTRIES_EAST + " text, "
                + COUNTRIES_NORTH + " text, "
                + COUNTRIES_SOUTH + " text);";
    }

    private static final String CREATE_TABLE_LAST_VISIT;

    static {
        CREATE_TABLE_LAST_VISIT = "create table " + TABLE_LAST_VISIT + " ("
                + BaseColumns._ID + " integer primary key autoincrement, "
                + LAST_VISIT_DATATIME + " text);";
    }


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        first_visit = context.getResources().getString(R.string.first_visit);
        did_not_saved = context.getResources().getString(R.string.did_not_save_countries);
        not_found = context.getResources().getString(R.string.isnt_found_coordinates);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version,
                          DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void addCountry(Details details) {
        db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COUNTRIES_NAME, details.getName());
        cv.put(COUNTRIES_CODE, details.getCode());
        cv.put(COUNTRIES_STATES_NAME, details.getStName());
        cv.put(COUNTRIES_STATES_CODE, details.getStCode());
        cv.put(COUNTRIES_LAT, details.getLat());
        cv.put(COUNTRIES_LON, details.getLon());
        cv.put(COUNTRIES_WEST, details.getWest());
        cv.put(COUNTRIES_EAST, details.getEast());
        cv.put(COUNTRIES_NORTH, details.getNorth());
        cv.put(COUNTRIES_SOUTH, details.getSouth());

        db.insert(TABLE_COUNTRIES, null, cv);
        db.close();
    }

    public void readLog(Cursor c) {
        int i = 0;
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                String str;
                do {
                    ++i;
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(Constants.LOG_TAG, i + ": " + str);
                } while (c.moveToNext());
            }
        } else
            Log.d(Constants.LOG_TAG, "Cursor == 0");
    }

    public boolean dublicateCountry(Details details) {
        db = this.getWritableDatabase();
        boolean check = true;
        String countryCode = details.getCode();
        String stateCode = details.getStCode();
        Cursor country = db.query(TABLE_COUNTRIES, null, COUNTRIES_CODE + " = ?" + " AND " +
                COUNTRIES_STATES_CODE + " = ?", new String[]{countryCode, ""}, null, null, null);
        if (country != null) {
            if (country.getCount() > 0) {
                check = false;
            }
        }
        country.close();
        Cursor state = db.query(TABLE_COUNTRIES, null, COUNTRIES_CODE + " = ?" + " AND " +
                COUNTRIES_STATES_CODE + " = ?", new String[]{countryCode, stateCode}, null, null, null);
        if (state != null) {
            if (state.getCount() > 0) {
                check = false;
            } else {
                check = true;
            }
        }
        state.close();
        db.close();
        return check;
    }

    public ArrayList<String> getAlarmCountryList() {
        db = this.getWritableDatabase();
        ArrayList<String> details = new ArrayList<>();
        Random rand = new Random();
        int number;
        Cursor country = db.query(TABLE_COUNTRIES, null, null, null, null, null, null);
        if (country != null) {
            if (country.getCount() > 0) {
                number = rand.nextInt(country.getCount());
                if (country.getCount() == number) {
                    number -= 1;
                }
                if (number < 0) {
                    number = 0;
                }
                country.moveToPosition(number);
                details.add(country.getString(country.getColumnIndex(COUNTRIES_NAME)));
                details.add(country.getString(country.getColumnIndex(COUNTRIES_CODE)));
            }
        }
        country.close();
        db.close();
        return details;
    }

    public List<Countries> getCountiesList() {
        db = this.getWritableDatabase();
        List<Countries> countriesList = new ArrayList<>();

        Cursor country = db.query(TABLE_COUNTRIES, null, COUNTRIES_STATES_CODE + " = ?", new String[]{""}, null, null,
                null);
        String countryName;
        String countryCode;
        String stateName;
        String stateCode;
        if (country != null) {
            if (country.getCount() > 0) {
                if (country.moveToFirst()) {
                    do {
                        countryName = country.getString(country.getColumnIndex(COUNTRIES_NAME));
                        countryCode = country.getString(country.getColumnIndex(COUNTRIES_CODE));
                        Cursor state = db.query(TABLE_COUNTRIES, null, COUNTRIES_CODE + " = ?" + " AND " +
                                COUNTRIES_STATES_CODE + " <> ?", new String[]{countryCode, ""}, null, null, null);
                        List<States> states = new ArrayList<>();
                        if (state != null) {
                            if (state.getCount() > 0) {
                                if (state.moveToFirst()) {
                                    do {
                                        stateName = state.getString(state.getColumnIndex(COUNTRIES_STATES_NAME));
                                        stateCode = state.getString(state.getColumnIndex(COUNTRIES_STATES_CODE));
                                        states.add(new States(stateName, stateCode));
                                    } while (state.moveToNext());
                                }
                            }
                        }
                        state.close();
                        countriesList.add(new Countries(countryName, countryCode, states));
                    } while (country.moveToNext());
                }
            }
        }
        if (countriesList.size() == 0) {
            countriesList.add(new Countries(did_not_saved, "", new ArrayList<States>()));
        }
        country.close();
        db.close();
        return countriesList;
    }

    //country = 0
    //states = 1
    public Details getDetailsCountry(String code, int countryOrStates) {
        db = this.getWritableDatabase();
        Details details = new Details();
        String query;
        if (countryOrStates == 0) {
            query = COUNTRIES_CODE;
        } else {
            query = COUNTRIES_STATES_CODE;
        }
        Cursor c = db.query(TABLE_COUNTRIES, null, query + " = ?", new String[]{code}, null, null, null);
        if (c.getCount() > 0) {
            c.moveToPosition(c.getCount() - 1);
            details.setLat(c.getString(c.getColumnIndex(COUNTRIES_LAT)));
            details.setLon(c.getString(c.getColumnIndex(COUNTRIES_LON)));
            details.setWest(c.getString(c.getColumnIndex(COUNTRIES_WEST)));
            details.setEast(c.getString(c.getColumnIndex(COUNTRIES_EAST)));
            details.setNorth(c.getString(c.getColumnIndex(COUNTRIES_NORTH)));
            details.setSouth(c.getString(c.getColumnIndex(COUNTRIES_SOUTH)));
        } else {
            details.setLat("");
            details.setLon("");
            details.setWest("");
            details.setEast("");
            details.setNorth("");
            details.setSouth("");
        }
        c.close();
        db.close();
        return details;
    }

    public String getLastVisit() {
        db = this.getWritableDatabase();
        String lastVisit = "";
        Cursor c = db.query(TABLE_LAST_VISIT, null, null, null, null, null, null);
        if (c.getCount() > 0) {
            c.moveToPosition(c.getCount() - 1);
            lastVisit = c.getString(c.getColumnIndex(LAST_VISIT_DATATIME));
        }
        c.close();
        db.close();
        return lastVisit;
    }

    public void setLastVisit(String lastVisit) {
        db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LAST_VISIT_DATATIME, lastVisit);
        db.update(TABLE_LAST_VISIT, cv, LAST_VISIT_ID + "=" + 1, null);
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_COUNTRIES);
        db.execSQL(CREATE_TABLE_LAST_VISIT);
        ContentValues cv = new ContentValues();
        cv.put(LAST_VISIT_DATATIME, first_visit);
        db.insert(TABLE_LAST_VISIT, null, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXIST " + TABLE_COUNTRIES);
        db.execSQL("DROP TABLE IF IT EXIST " + TABLE_LAST_VISIT);
        onCreate(db);
    }
}