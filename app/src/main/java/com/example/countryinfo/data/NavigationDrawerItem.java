package com.example.countryinfo.data;

public class NavigationDrawerItem {
    private boolean showNotify;
    private String title;
    private int image;


    public NavigationDrawerItem() {

    }

    public NavigationDrawerItem(boolean showNotify, String title, int image) {
        this.showNotify = showNotify;
        this.title = title;
        this.image = image;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
