package com.example.countryinfo.data;

import android.graphics.Bitmap;

public class Details {
    private String name;
    private String code;
    private String stName;
    private String stCode;
    private String lat;
    private String lon;
    private String west;
    private String east;
    private String north;
    private String south;
    private Bitmap flag;
    public Details() {
    }

    public Details(String name, String code, String stName, String stCode, String lat, String lon, String west,
                   String east, String north, String south, Bitmap flag) {
        this.name = name;
        this.code = code;
        this.stName = stName;
        this.stCode = stCode;
        this.lat = lat;
        this.lon = lon;
        this.west = west;
        this.east = east;
        this.north = north;
        this.south = south;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getWest() {
        return west;
    }

    public void setWest(String west) {
        this.west = west;
    }

    public String getEast() {
        return east;
    }

    public void setEast(String east) {
        this.east = east;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String north) {
        this.north = north;
    }

    public String getSouth() {
        return south;
    }

    public void setSouth(String south) {
        this.south = south;
    }

    public String getStName() {
        return stName;
    }

    public void setStName(String stName) {
        this.stName = stName;
    }

    public String getStCode() {
        return stCode;
    }

    public void setStCode(String stCode) {
        this.stCode = stCode;
    }

    public Bitmap getFlag() {
        return flag;
    }

    public void setFlag(Bitmap flag) {
        this.flag = flag;
    }
}
