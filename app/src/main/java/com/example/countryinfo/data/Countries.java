package com.example.countryinfo.data;

import java.util.ArrayList;
import java.util.List;

public class Countries {
    private String name;
    private String code;
    private List<States> states = new ArrayList<>();

    public Countries(String name, String code, List<States> states){
        this.name = name;
        this.code = code;
        this.states = states;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<States> getStates() {
        return states;
    }

    public void setStates(List<States> states) {
        this.states = states;
    }
}

