package com.example.countryinfo.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.countryinfo.R;
import com.example.countryinfo.data.Countries;
import com.example.countryinfo.data.States;

import java.util.ArrayList;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<Countries> mCountriesList;
    private List<Countries> mOriginalList;
    private static final int[] EMPTY_STATE_SET = {};
    private static final int[] GROUP_EXPANDED_STATE_SET = {android.R.attr.state_expanded};
    private static final int[][] GROUP_STATE_SETS = {
            EMPTY_STATE_SET, // 0
            GROUP_EXPANDED_STATE_SET // 1
    };

    public ExpandableListAdapter(Context c, List<Countries> countriesList) {
        this.mContext = c;
        this.mCountriesList = new ArrayList<Countries>();
        this.mCountriesList.addAll(countriesList);
        this.mOriginalList = new ArrayList<Countries>();
        this.mOriginalList.addAll(countriesList);
    }

    @Override
    public int getGroupCount() {
        return mCountriesList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<States> statesList = mCountriesList.get(groupPosition).getStates();
        return statesList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mCountriesList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<States> statesList = mCountriesList.get(groupPosition).getStates();
        return statesList.get(childPosition);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Countries countries = (Countries) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.ext_listview_group, null);
        }
        View ind = convertView.findViewById(R.id.explist_indicator);
        if (ind != null) {
            ImageView indicator = (ImageView) ind;
            if (getChildrenCount(groupPosition) == 0) {
                indicator.setVisibility(View.INVISIBLE);
            } else {
                indicator.setVisibility(View.VISIBLE);
                int stateSetIndex = (isExpanded ? 1 : 0);
                Drawable drawable = indicator.getDrawable();
                drawable.setState(GROUP_STATE_SETS[stateSetIndex]);
            }
        }

        TextView tvHeading = (TextView) convertView.findViewById(R.id.heading);
        tvHeading.setText(countries.getName().trim());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        States states = (States) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.ext_listview_row, null);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.name);
        tvName.setText(states.getName().trim());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void filterData(String query) {

        query = query.toLowerCase();
        mCountriesList.clear();

        if (query.isEmpty()) {
            mCountriesList.addAll(mOriginalList);
        } else {

            for (Countries countries : mOriginalList) {
                if (countries.getName().toLowerCase().contains(query)) {
                    Countries newCountries = new Countries(countries.getName(), countries.getCode(), countries
                            .getStates());
                    mCountriesList.add(newCountries);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
